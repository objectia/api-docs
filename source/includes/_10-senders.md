# Senders

## List

```javascript
mailshake.senders.list({
  search: '@gmail.com'
})
  .then(result => {
    result.results.forEach(campaign => {
      console.log(JSON.stringify(campaign, null, 2));
    });
  })
  .catch(err => {
    console.error(`${err.code}: ${err.message}`);
  });
```

```shell
curl "https://api.mailshake.com/2017-04-01/senders/list" \
  -u "my-api-key:" \
  -d search="@gmail.com"
```

> This endpoint returns [paginated](#Pagination) [Sender](#Sender) models.

List all of a team's senders.

### Parameters

Parameter | Default | Required | Description
--------- | ------- | -----------
search |  | No | Filters what senders are returned.
nextToken |  | No | Fetches the next page from a previous request.
perPage | 100 | No | How many results to get at once, up to 100.
