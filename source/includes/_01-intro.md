# Introduction

Welcome to the Objectia API. 


Official libraries for the Objectia API are [available in several languages](). 



## Endpoints

All API requests are made to <code>api.objectia.com</code> and all requests are served over HTTPS. Calls made over plain HTTP will fail. 

The current API version is v1.

> API Endpoint

```http
https://api.objectia.com/v1
```


## Versioning

As we develop future versions of our API that are not backwards-compatible, we will leave the old version running and create a new url for the latest version. We will retain support for obsolete versions for a generous period of time and will send email notifications of any changes.

Future versions will be v2, v3, etc.


## Authentication

All requests to the API must be authenticated with an access token. You can submit the access token in the HTTP request header, or you can specify the access token as a HTTP URL query parameter.

> In the HTTP Authorization header:

```http
Authorization: "Bearer d699e37f506f49f1aa15105228833cd7"
```

> In the URL as the access_token parameter:

```bash
https://api.objectia.com/v1/geoip/8.8.8.8?api_key=d699e37f506f49f1aa15105228833cd7
```

<aside class="notice">If you do not have an API Key, you can easily generate one by heading over to the [Account page](https://objectia.com/account).</aside>

API requests without authentication will fail.


## Rate limits

API access rate limits are applied at a per-key basis in unit time. Access to the API using a key is limited to 60 requests per minute. In addition, every API response is accompanied by the following set of headers to identify the status of your consumption.

Header | Description
------ | -----------
<span style="white-space: nowrap;"><code>X&#8209;RateLimit&#8209;Limit</code></span> | The maximum number of requests that the consumer is permitted to make per minute.
<span style="white-space: nowrap;"><code>X&#8209;RateLimit&#8209;Remaining</code></span> | The number of requests remaining in the current rate limit window.
<span style="white-space: nowrap;"><code>X&#8209;RateLimit&#8209;Reset</code></span> | The time at which the current rate limit window resets in UTC epoch seconds.

Once you hit the rate limit, you will receive a response similar to the following JSON, with a status code of <code>429 Too Many Requests</code>.

```json
{
  "status": 429,
  "success": false,
  "message": "Rate Limit exceeded"
}
```

## Errors

Objectia uses conventional HTTP response codes to indicate the success or failure of an API request. In general: Codes in the <code>2XX</code> range indicate success. Codes in the <code>4XX</code> range indicate an error that failed given the information provided (e.g., a required parameter was omitted, a charge failed, etc.). Codes in the <code>5XX</code> range indicate an error with Objectia's servers (these are rare).


## Making requests

The API only supports JSON at present.

> The Content-Type header:

```http
Content-Type: "application/json"
```

You must supply a <code>Content-Type: application/json</code> header in <code>PUT</code> and <code>POST</code> requests. You must set an <code>Accept: application/json</code> header on all requests. 


## Responses

> Single-item responses will be in this format:

```json
{
  "status": 200,
  "success": true,
  "data": {
    "id": 1,
    "some": "more data ..."
  }
}
```

Most endpoints return a single model of data. 

> Multi-item responses will be in this format:

```json
{
  "status": 200,
  "success": true,
  "data": [
    { 
      "id": 1,
      "some": "more data ..."
    },
    { 
      "id": 2,
      "some": "more data ..."
    }
  ]
}
```

<!--## Pagination ???

> Paginated data will look like this:

```json
{
  "nextToken": "...",
  "results": [
    { ... },
    { ... }
  ]
}
```

> Get the next page of data like so:

```go
package main

import "api"

func main() {
  // comment
  api.Auth("bababab")
}
```

```javascript
mailshake.campaigns.list()
  .then(result => {
    console.log(`Page 1: ${JSON.stringify(result.results, null, 2)}`);
    // Just call `next` on the result to fetch the next page.
    return result.next();
  })
  .then(result => {
    console.log(`Page 2: ${JSON.stringify(result.results, null, 2)}`);
  });
```

```curl
curl "https://api.objectia.com/v1/example" \
  -u "my-api-key:" \
  -d nextToken=...
```

Endpoints that return multiple results will include a `nextToken` parameter that you can pass into another request to fetch the next results. These endpoints will also accept a `perPage` parameter to control the size of the record sets.

If `nextToken` is null then you're looking at the last page of data.

-->


## Legal notices

Your use and access to the API is expressly conditioned on your compliance with the policies, restrictions, and other provisions related to the API set forth in our [API License Agreement](https://objectia.com/api-license) and the [Privacy Policy](https://objectia.com/privacy), in all uses of the API. If Objectia believes that you have or attempted to violate any term, condition, or the spirit of these policies or agreements, your right to access and use the API may be temporarily or permanently revoked.