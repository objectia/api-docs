# Geo IP API

The Geo IP API ....

## <span class="get">GET</span> Location by IP

<code class="heading">https://api.objectia.com/v1/geoip/:ip</code>

Retrieves the geographical location for a given IP address or domain name.

> <div style="font-size:24px; margin-bottom: 25px">Get IP Location</div>

<!--
> Usage:

```curl
GET /v1/geoip/:ip
```

```csharp
client.GetIPLocation()
```

```go
client.GetIPLocation()
```

```java
client.GetIPLocation()
```

```javascript
client.GetIPLocation()
```

```php
client.GetIPLocation()
```

```python
objectia.GetIPLocation()
```

```ruby
Objectia::client.get_ip_location
```

```swift
client.GetIPLocation()
```
-->

> Example request:

```curl
curl https://api.objectia.com/v1/geoip/8.8.8.8 \
  -H "Authorization: Bearer YOUR-API-KEY" \
  -H "Accept: application/json"
```

```go
package main

import (
  "fmt"
  objectia "github.com/objectia/objectia-go"
)

func main() {
  apiKey := "YOUR-API-KEY"
  client, err := api.NewClient(apiKey, nil)
  if err != nil {
    panic(err)
  }
  result, err := client.GetIPLocation("8.8.8.8")
  if err != nil {
    panic(err)
  }
  fmt.Printf("Location: %+v\n", result)
}
```


> Example response:

```json
{
  "data": {
    "ip": "8.8.8.8",
    "type": "ipv4",
    "continent_name": "North America",
    "continent_code": "NA",
    "country_name": "United States",
    "country_name_native": "United States",
    "country_code": "US",
    "country_code3": "USA",
    "capital": "Washington D.C.",
    "region_name": "California",
    "region_code": "CA",
    "city": "Mountain View",
    "postcode": "94043",
    "latitude": 37.405992,
    "longitude": -122.078515,
    "phone_prefix": "+1",
    "currencies": [
      {
        "code": "USD",
        "num_code": "840",
        "name": "US Dollar",
        "name_plural": "US dollars",
        "symbol": "$",
        "symbol_native": "$",
        "decimal_digits": 2
      }
    ],
    "languages": [
      {
        "code": "eng",
        "code2": "en",
        "name": "English",
        "native_name": "English",
        "rtl": false
      }
    ],
    "flag": "https://cdn.objectia.com/assets/flags/us.svg",
    "flag_emoji": "🇺🇸",
    "is_eu": false,
    "internet_tld": ".us",
    "timezone": {
      "id": "America/Los_Angeles",
      "localtime": "2019-02-19T08:56:28-08:00",
      "gmt_offset": -28800,
      "code": "PST",
      "daylight_saving": true
    }
  },
  "privacy": "https://objectia.com/privacy",
  "status": 200,
  "success": true,
  "terms": "https://objectia.com/terms"
}
```

Parameter | Value
------ | -----------
ip<br/><span class="required" /> | The IP address or domain name to look up.

