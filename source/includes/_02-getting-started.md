# Getting Started

In this section we will ...


## Resources

* Learn about the [most common tasks]() performed with the Objectia API.
* Try tutorials and learn how to use the Objectia APIs in the [Help Center](https://help.objectia.com).
* Bookmark the [API documentation](https://docs.objectia.com). It's your reference for all the APIs and endpoints.
* Try out various API requests from the command line. All the examples in the API docs are in cURL.
* Ask questions in the [community forum](https://objectia-community.slack.com).
* Subscribe to announcements and breaking change notices to stay on top of the latest developments.


## API basics tutorials

* [Making requests to the Objectia API](https://help.objectia.com/docs/xxxx)
* [Paginating through lists](https://help.objectia.com/docs/xxxx)


## API clients

* Speed up development by using an [API client](https://objectia.com/apiclients) for Android, C#, Go, Java, Javascript, NodeJS, PHP, Python, Ruby, Swift, and others.

