# Authentication

Most of the apps authorized to use the Mailshake API can only access their own team's data. For these apps, you can use our simple authentication. Any app can use our OAuth version 2 authentication.

<aside class="notice">If your app will access data for other teams OAuth is the only option.</aside>

## Simple

```javascript
var mailshake = require('mailshake-node')('my-api-key');
mailshake.me()
  .then(result => {
    console.log(JSON.stringify(result, null, 2));
  })
  .catch(err => {
    console.error(`${err.code}: ${err.message}`);
  });
```

```shell
# curl uses the -u flag to pass basic auth credentials
# (adding a colon after your API key prevents cURL from asking for a password).
curl "https://api.mailshake.com/2017-04-01/me" \
  -u "my-api-key:"
```

Simply include your API key as a querystring parameter (`apiKey`), part of your body json  (`apiKey`), or via an http `Authorization` header that looks like:

`Authorization: Basic [base-64 encoded version of your api key]`

> Make sure to replace `my-api-key` with your API key.

## OAuth2

```javascript
// mailshake-node has hooks to support most any OAuth library.
// You can either customize the request with `customizeRequest`
// or outright replace how the request is made with `overrideCreateRequest`

var mailshake = require('mailshake-node')({
  customizeRequest(options) => {
    // options.headers.authorization = [...oauth header...]
    return options;
  }),

  // or

  overrideCreateRequest(options, callbackFn) => {
    // Create a standard node request via an OAuth tool
    // Something that looks like this:
    return https(options, callbackFn);
  })
});
```

If your app has been approved as a 3rd-party app (one in which you can access other users' data instead of limited of just your own team), you must use OAuth V2 to integrate with us. We'll deliver your consumer key and secret to you manually. The other applicable OAuth settings are below:

Authorization url:
`https://mailshake.com/connect/`

Access token url:
`https://api.mailshake.com/2017-04-01/token`

Refresh token url:
`https://api.mailshake.com/2017-04-01/token`

### OAuth scope

Specify the OAuth scope to tell customers what permissions you're requesting. Specify the scopes in a comma-delimited string like so: `campaign-read,campaign-write`

Scope | Description
--------- | -------
campaign-read | Grants read access to all operations.
campaign-write | Grants write access to all operations.

## Test connection

```javascript
mailshake.me()
  .then(result => {
    console.log(JSON.stringify(result, null, 2));
  })
  .catch(err => {
    console.error(`${err.code}: ${err.message}`);
  });
```

```shell
curl "https://api.mailshake.com/2017-04-01/me" \
  -u "my-api-key:"
```

> This endpoint returns a simple object with a [User](#User) model attached.

```json
{
  "user": "[User model]"
}
```

You can hit our `/me` endpoint to test that authentication is working. It will return information about the current user you are authenticating as.
