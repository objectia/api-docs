---
title: Objectia API Reference

language_tabs:
  - curl: cURL
  - csharp: C#
  - go: Go
  - java: Java
  - javascript: Javascript
  - php: PHP
  - python: Python
  - ruby: Ruby
  - swift: Swift

#toc_footers:
#  - © 2018 UAB Salesfly. All rights reserved.

includes:
  - 01-intro
  - 02-getting-started
  - 03-geoip
#  - 04-currency
#  - 05-tax
#  - 02-auth
#  - 04-campaigns
#  - 05-recipients-add
#  - 06-recipients-other
#  - 07-activity
#  - 08-leads
#  - 09-team
#  - 10-senders
#  - 80-push
#  - 89-models
#  - 95-errors
#  - 99-limits

search: false
---
